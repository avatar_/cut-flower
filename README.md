
# Cut flower management system

`Cut flower management system` is a inventory control system which can be used to manage day to day business in a cut flower business.

Cut flower management system has been developed with a real requirement from a cut flower farm owner.

## Getting started
Project has been developed using Netbeans IDE, therefor project needs to be opened from Netbeans IDE.  

## Prerequisite

User should have following requirements to run the project successfully.

* Mysql version 5.6.40 or above

* Database for cut flower

* Tables created to save the data

\* Database script can be obtained by sending an email to the **niroshan.kdt@gmail.com**.

### Third party libraries

Please add the following libraries to the project's build path.

* jcalendar-1.3.3
* poi-3.11
* poi-excelant-3.11
* poi-ooxml-3.11
* poi-ooxml-schemas-3.11
* poi-scratchpad-3.11
* xmlbeans-2.6.0
* ommons-beanutils-1.8.2
* commons-collections-3.2.1
* commons-digester-2.1
* commons-logging-1.1
* groovy-all-2.0.1
* iText-2.1.7.js2
* ijasperreports-5.6.0
* jfreechart-1.0.12
* poi-3.7
* weblaf-complete-1.28
* 8seaglasslookandfeel-0.2
* WebLookAndFeel
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.DAO;

import com.cic.data.DBConnManager;
import com.cic.data.Messages;
import com.cic.entity.PackingMaterial;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author NIroshan
 */
public class PackingMaterialDAO {

    DBConnManager dbcon;

    public PackingMaterialDAO() {
        dbcon = new DBConnManager();
    }

    public boolean save(PackingMaterial pm) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;

        try {

            String id = pm.getID();
            String name = pm.getName();
            int quantity = pm.getQuantity();

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "INSERT INTO packingmaterial(id,name,quantity)"
                    + " VALUES('" + id + "','" + name + "'," + quantity + " )";

            int i = stmt.executeUpdate(sql);

            if (i > 0) {
                saved = true;
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return saved;
    }

    public boolean delete(PackingMaterial pm) {
        boolean delete = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String id = pm.getID();
            
            String sql = "DELETE FROM packingmaterial WHERE id='"+id+"'  ";
            stmt.executeUpdate(sql);
            delete = true;

        }
        catch(MySQLIntegrityConstraintViolationException ex)
        {
            Messages.showError("Detetion error", "Cannot delete selected record.This record refere to another tables.\nDeletion of this record may lead to data loss");
        }
        catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return delete;
    }

    public boolean update(PackingMaterial pm) {
        boolean update = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String id = pm.getID();
            String name = pm.getName();
            int quantity = pm.getQuantity();
            
            String sql = "UPDATE packingmaterial SET name = '"+name+"',quantity="+quantity+" WHERE id='"+id+"'   ";
            stmt.executeUpdate(sql);
            update = true;

        }  
        
        catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return update;
    }

    public boolean existing(PackingMaterial pm) {
        boolean existing = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String name = pm.getName();
            String sql = "SELECT * FROM packingmaterial WHERE name ='"+name+"' ";
            
            ResultSet rst = stmt.executeQuery(sql);
            if(rst.next())
            {
                existing = true;
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return existing;
    }

    public Vector findPMID(String id) {

        Vector data = new Vector();
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT name,id FROM packingmaterial WHERE name LIKE '" + id + "%' ";
            ResultSet rst = stmt.executeQuery(sql);
            while (rst.next()) {
                data.add(rst.getString(2)+","+rst.getString(1));
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return data;
    }

    public String getName(String id) {
        String name = "";
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT name FROM packingmaterial WHERE id='" + id + "' ";
            ResultSet rst = stmt.executeQuery(sql);
            if (rst.next()) {
                System.out.println(rst.getString(1));
                name = rst.getString(1).toString();
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return name;
    }

    public Vector getAllPackingMaterials()
    {
        Vector data = new Vector();
        
         Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id,name,quantity FROM packingmaterial ";
            ResultSet rst = stmt.executeQuery(sql);
            while (rst.next()) {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));
                
                data.add(v);
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return data;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.DAO;

import com.cic.data.DBConnManager;
import com.cic.entity.Sales;
import com.cic.entity.SalesDetail;
import com.cic.entity.PackingMaterial;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author NIroshan
 */
public class SalesDAO {
    
     DBConnManager dbcon;

    public SalesDAO() {
        dbcon = new DBConnManager();
    }

    public boolean save(Sales sales) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql1,sql2;
            
            //export data
            String date = sales.getDate();
            double grandTotal = sales.getTotal();
            int client = sales.getClient().getId();
            String type = sales.getType();
            
            sql1="INSERT INTO sales(date,total,client,type) "
                    + "VALUES('"+date+"',"+grandTotal+","+client+",'"+type+"')";
            stmt.addBatch(sql1);
            
            ArrayList<SalesDetail> salesDetails = sales.getSalesDetails();
            
            
            for(SalesDetail detail : salesDetails)
            {
                String pid = detail.getVariety().getID();
                String length = detail.getVariety().getLength();
                int quantity = detail.getVariety().getQuantity();
                double unitPrice = detail.getVariety().getUnitPrice();
                double total = detail.getTotal();
                String vtype = detail.getVariety().getType();
                int sid = detail.getSupply().getId();
                
                sql2 = "INSERT INTO s_details(id,pid,type,length,quantity,unitPrice,total,SID) "
                        + "VALUES(LAST_INSERT_ID(),'"+pid+"','"+vtype+"','"+length+"',"+quantity+","+unitPrice+","+total+","+sid+"   )";
                stmt.addBatch(sql2);
                
            }
            
            stmt.executeBatch();
            int[] i = stmt.executeBatch();
            saved = true;
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return saved;
    }
    
    public boolean update(Sales sales) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql1,sql2;
            
            //export data
            double grandTotal = sales.getTotal();
            int id = sales.getId();
            
            
            sql1="UPDATE sales SET total ="+grandTotal+" "
                    + "WHERE id = "+id+" ";
            stmt.addBatch(sql1);
            
            ArrayList<SalesDetail> salesDetails = sales.getSalesDetails();
            
            
            for(SalesDetail detail : salesDetails)
            {
                String pid = detail.getVariety().getID();
                String length = detail.getVariety().getLength();
                int quantity = detail.getVariety().getQuantity();
                double unitPrice = detail.getVariety().getUnitPrice();
                double total = detail.getTotal();
                int sid = detail.getSupply().getId();
                
                sql2 = "INSERT INTO s_details(id,pid,length,quantity,unitPrice,total,SID) "
                        + "VALUES( "+id+",'"+pid+"','"+length+"',"+quantity+","+unitPrice+","+total+","+sid+") "
                        + "ON DUPLICATE KEY UPDATE quantity = "+quantity+" ,unitPrice = "+unitPrice+",total= "+total+"  ";
                stmt.addBatch(sql2);
                
            }
            
            stmt.executeBatch();
            int[] i = stmt.executeBatch();
            saved = true;
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return saved;
    }
    

    public boolean includePlantPurchases(Sales sales) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql1,sql2;
            
            int salesID = sales.getId();
            double grandTotal = sales.getTotal();
             
            sql1 = "UPDATE sales SET total = total +"+grandTotal+" WHERE  id = "+salesID+"";
            stmt.addBatch(sql1);
            
            ArrayList<SalesDetail> salesDetails = sales.getSalesDetails();
            
            
            for(SalesDetail detail : salesDetails)
            {
                String pid = detail.getVariety().getID();
                String length = detail.getVariety().getLength();
                int quantity = detail.getVariety().getQuantity();
                double unitPrice = detail.getVariety().getUnitPrice();
                double total = detail.getTotal();
                int sid = detail.getSupply().getId();
                
                sql2 = "INSERT INTO s_details(id,pid,length,quantity,unitPrice,total,SID) "
                        + "VALUES("+salesID+",'"+pid+"','"+length+"',"+quantity+","+unitPrice+","+total+","+sid+" ) "
                        + "ON DUPLICATE KEY UPDATE  quantity = "+quantity+",unitPrice="+unitPrice+",total="+total+"  ";
                stmt.addBatch(sql2);
                
            }
            
            stmt.executeBatch();
            int[] i = stmt.executeBatch();
            saved = true;
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return saved;
    }
    public ArrayList<ArrayList> getSales()
    {
        ArrayList<ArrayList> data = new ArrayList();
        
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql = "SELECT e.id,e.date,e.total,c.fname,c.lname,c.country "
                    + "FROM sales e LEFT JOIN client c ON e.client = c.id";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            while(rst.next())
            {
                ArrayList row = new ArrayList();
                row.add(rst.getString(1));
                row.add(rst.getString(2));
                row.add(rst.getString(3));
                row.add(rst.getString(4)+" "+rst.getString(5));
                row.add(rst.getString(6));
                
                data.add(row);
            }
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return data;
    }
    
    public ArrayList<ArrayList> getSales(String date,String type)
    {
        ArrayList<ArrayList> data = new ArrayList();
        
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql = "SELECT e.id,e.date,e.total,c.fname,c.lname,c.country "
                    + "FROM sales e LEFT JOIN client c ON e.client = c.id "
                    + "WHERE e.date = '"+date+"' AND e.type = '"+type+"' ";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            while(rst.next())
            {
                ArrayList row = new ArrayList();
                row.add(rst.getString(1));
                row.add(rst.getString(2));
                row.add(rst.getString(3));
                row.add(rst.getString(4)+" "+rst.getString(5));
                row.add(rst.getString(6));
                
                data.add(row);
            }
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return data;
    }
    
    public ArrayList<ArrayList> getSalesDetails(int  id,String type)
    {
        ArrayList<ArrayList> data = new ArrayList();
        
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = null ;
            if (type.equals("crop")) {

                sql = "SELECT s.id,s.date, es.name ,  ed.pid,p.name,ed.length,ed.quantity,ed.unitPrice,ed.total "
                        + "FROM sales e LEFT JOIN  e_details ed ON e.id = ed.id "
                        + "LEFT JOIN plant p ON ed.pid = p.id "
                        + "LEFT JOIN supply s ON ed.SID = s.id "
                        + "LEFT JOIN crop c ON s.id = c.id , estate es  "
                        + "WHERE ed.id = " + id + " AND es.id = c.eid AND s.type = 'crop'  ";
            }
            else if(type.equals("purchase"))
            {
                sql = "SELECT s.id,s.date,   ed.pid,p.name,ed.length,ed.quantity,ed.unitPrice,ed.total "
                        + "from sales e left join e_details ed on e.id = ed.id "
                        + "left join plant p on ed.pid = p.id "
                        + "left join supply s on ed.SID = s.id "
                        + "left join purchase c on s.id = c.id "
                        + "where ed.id = "+id+"   and s.type = 'purchase' ";
            }
            
            ResultSet rst = stmt.executeQuery(sql);
            
            if(type.equals("crop"))
            {
                 while(rst.next())
                {
                    ArrayList row = new ArrayList();
                    row.add(rst.getString(1) + ":-" + rst.getString(2)+","+rst.getString(3));
                    row.add(rst.getString(4) + ":" + rst.getString(5));

                    row.add(rst.getString(6));
                    row.add(rst.getString(7));
                    row.add(rst.getString(8));
                    row.add(rst.getString(9));
                    data.add(row);
                }
                
            }
            else if(type.equals("purchase"))
            {
                while(rst.next())
                {
                    ArrayList row = new ArrayList();
                    row.add(rst.getString(1) + ":-" + rst.getString(2));
                    row.add(rst.getString(3) + ":" + rst.getString(4));

                    row.add(rst.getString(5));
                    row.add(rst.getString(6));
                    row.add(rst.getString(7));
                    row.add(rst.getString(8));
                    data.add(row);
                }
            }
            
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return data;
    }
    
    public boolean includePackingMaterial(Sales sales) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String  sql2;
            
            //export data
            
            int salesID = sales.getId();
            
            
            ArrayList<PackingMaterial> packingMaterials = sales.getPackingMaterial();
            
            
            for(PackingMaterial detail : packingMaterials)
            {
                
                String packMatID = detail.getID();
                int quantity = detail.getQuantity();
                
                
                sql2 = "INSERT INTO sales_pm(export,pmid,quantity) "
                        + "VALUES("+salesID+",'"+packMatID+"',"+quantity+") "
                        + "ON DUPLICATE KEY UPDATE quantity = "+quantity+" ";
                stmt.addBatch(sql2);
                
            }
            
            stmt.executeBatch();
            int[] i = stmt.executeBatch();
            saved = true;
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return saved;
    }
    
    public ArrayList<ArrayList> getPackingMaterials(int eid)
    {
        ArrayList data = new ArrayList();
        
        
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql = "select epm.pmid,pm.name,epm.quantity "
                    + "from sales_pm epm,packingmaterial pm "
                    + "where epm.pmid = pm.id and epm.export = "+eid+"";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            while(rst.next())
            {
                ArrayList row = new ArrayList();
                row.add(rst.getString(1)+","+rst.getString(2));
                row.add(rst.getString(3));
                data.add(row);
            }

        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return data;
        
    }
    
    
    public int getNextJobID()
    {
        int jobID = 0;
        
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql1 = "SELECT MAX(id) FROM sales";
            
            ResultSet rst = stmt.executeQuery(sql1);
            
            if(rst.next())
                jobID = rst.getInt(1); 
            
            jobID++;
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        
        
        return jobID;
    }
    
     public Vector getSales(String from, String to, int cid) {
        Vector<Vector> sales = new Vector<>();

        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT e.id,e.date,e.total,c.fname,c.lname,c.country "
                    + "FROM  sales e,client c "
                    + "WHERE e.client = c.id AND e.date BETWEEN '" + from + "' AND '" + to + "' AND c.id = " + cid + "  ;";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));
                v.add(rst.getString(4) + " " + rst.getString(5));
                v.add(rst.getString(6));

                sales.add(v);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return sales;
    }
     
      public Vector getSalesVector() {
        Vector<Vector> sales = new Vector<>();

        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT e.id,e.date,e.total,c.fname,c.lname,c.country "
                    + "FROM  sales e,client c Where e.client = c.id;";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));
                v.add(rst.getString(4) + " " + rst.getString(5));
                v.add(rst.getString(6));

                sales.add(v);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return sales;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cic.DAO;

import com.cic.data.DBConnManager;
import com.cic.entity.Estate;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author NIroshan
 */
public class EstateDAO {
    
    DBConnManager dbcon;

    public EstateDAO() {
        dbcon = new DBConnManager();
    }
    
    
    
    public boolean save(Estate estate) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            String name = estate.getName();
            String sql = "INSERT INTO estate(name) VALUES('"+name+"')";
            stmt.executeUpdate(sql);
            saved = true;
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return saved;
    }
    
    public boolean delete() {
        boolean delete = false;
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "";
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return delete;
    }
    
    public boolean update() {
        boolean update = false;
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "";
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return update;
    }
    
    
    public boolean existing() {
        boolean existing = false;
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "";
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return existing;
    }
    
    public Vector getAllEstates()
    {
        Vector branches = new Vector();
        
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id,name FROM estate";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                branches.add(v);
               
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return branches;
    }
    
    public Vector getAllEstatesIDNames()
    {
        Vector branches = new Vector();
        
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id,name FROM estate";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                branches.add(rst.getString(1)+":"+rst.getString(2));
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return branches;
    }

    public int getEstateID(String name) {
        int id = 0;
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id FROM branch WHERE name= '"+name+"' ";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            if(rst.next())
            {
                id = Integer.parseInt(rst.getString(1));
            }
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return id;
    }
}

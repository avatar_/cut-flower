/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.DAO;

import com.cic.data.DBConnManager;
import com.cic.entity.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author NIroshan
 */
public class SupplyDAO {
     DBConnManager dbcon;

    public SupplyDAO() {
        dbcon = new DBConnManager();
    }
    
    public boolean save(Supply supply) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql2;
            String sql1 ;
            
            String date = supply.getDate();
            String type = supply.getType();

            String sql = "INSERT INTO supply(date,type) VALUES('" + date + "','" + type + "'  )";
            stmt.addBatch(sql);
            
            if(type.equals("crop"))
            {
                int estate = supply.getCrop().getEstate().getId();
                sql1 = "INSERT INTO crop(id,eid) VALUES(LAST_INSERT_ID(),"+estate+")";
                stmt.addBatch(sql1);
                
                ArrayList<CropDetail> cropDetails = supply.getCrop().getCropDetails();

                for (int i = 0; i < cropDetails.size(); i++) {
                    String pid = cropDetails.get(i).getVariety().getID();
                    int quantity = cropDetails.get(i).getVariety().getQuantity();
                    String vtype = cropDetails.get(i).getVariety().getType();

                    sql2 = "INSERT INTO crop_details(id,pid,type,quantity,remaining) VALUES(LAST_INSERT_ID(),'" + pid + "','"+vtype+"'," + quantity + ","+quantity+")";
                    stmt.addBatch(sql2);
                }
            }
            else if(type.equals("purchase"))
            {
                
                double grandTotal  = supply.getPurchase().getTotal();
                int sid = supply.getPurchase().getSid();
                
                sql1 ="INSERT INTO purchase(id,total,sid) "
                        + "VALUES(LAST_INSERT_ID(),"+grandTotal+","+sid+" )";
                
                stmt.addBatch(sql1);
                Purchase purchase = supply.getPurchase();
                ArrayList<PurchaseDetail> purchaseDettails = purchase.getPurchaseDetail();
                
                for(PurchaseDetail detail : purchaseDettails)
                {
                    Variety variety = detail.getVariety();
                    String pid = variety.getID();
                    String vType = variety.getType();
                    
                    String length = variety.getLength();
                    int quantity = variety.getQuantity();
                    double unitPrice = variety.getUnitPrice();
                    double total = detail.getTotal();
                    
                    sql2 = "INSERT INTO purchase_details(id,pid,type,length,quantity,unit_price,total,remaining) "
                            + "VALUES(LAST_INSERT_ID(),'"+pid+"','"+vType+"','"+length+"',"+quantity+","+unitPrice+","+total+","+quantity+")";
                    
                    
                    
                    stmt.addBatch(sql2);
                }
            }

            stmt.executeBatch();

            saved = true;

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return saved;
    }
    
    
    
    public Vector getCrops()
    {
        Vector supplies = new Vector();
        
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT s.id,s.date,e.name FROM supply s,crop c,estate e "
                    + "WHERE s.type= 'crop' AND s.id = c.id AND c.eid = e.id ";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                supplies.add(rst.getString(1)+":-"+rst.getString(2)+","+rst.getString(3));
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return supplies;
    }
    public Vector getCrops(String date)
    {
        Vector supplies = new Vector();
        
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT s.id,s.date  FROM supply s "
                    + "WHERE    s.date = '"+date+"'  ";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                supplies.add(String.format("%03d", rst.getInt(1))+":-"+rst.getString(2));
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return supplies;
    }
    public Vector getPurchases()
    {
        Vector supplies = new Vector();
        
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id,date FROM supply WHERE type= 'purchase' ";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                supplies.add(rst.getString(1)+":-"+rst.getString(2));
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return supplies;
         
    }
    
     public ArrayList  getPurchases(String date)
    {
        ArrayList data = new ArrayList();
        
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql1 = "SELECT p.id,su.date,s.fname,s.lname "
                    + "FROM purchase p , supplier s ,supply su "
                    + "WHERE p.sid = s.id AND su.id = p.id AND su.date = '"+date+"' ";
            ResultSet rst = stmt.executeQuery(sql1);
            
            while(rst.next())
            {
               
                data.add(rst.getString(1)+":-"+rst.getString(2)+","+rst.getString(3)+" "+rst.getString(4));
            }
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return data;
    }
     
     public ArrayList<ArrayList> getCropDetails(int id)
     {
         ArrayList<ArrayList> data = new ArrayList<>();
         
         Connection con = null;
         Statement stmt;
         
         try {
             
             con = dbcon.getConnect();
             stmt = con.createStatement();
             
             String sql1 = "SELECT cd.pid,p.name,cd.quantity,cd.remaining "
                     + "FROM crop_details cd LEFT JOIN plant p ON cd.pid=p.id "
                     + "WHERE cd.id = "+id+" ";
             ResultSet rst = stmt.executeQuery(sql1);
             
             while(rst.next())
             {
                 ArrayList row = new ArrayList();
                 
                 row.add(rst.getString(1)+":"+rst.getString(2));
                 row.add(rst.getString(3));
                 row.add(rst.getString(4));
                 data.add(row);
             }
         }
         catch (Exception e) {
             System.out.println(e);
         }
         finally {
             if (con != null) {
                 dbcon.closeConnection(con);
             }
         }
         
         
         return data;
     }
     
     public ArrayList<ArrayList> getPurchaseDetails(int id)
     {
         ArrayList<ArrayList> data = new ArrayList<>();
         
         Connection con = null;
         Statement stmt;
         
         try {
             
             con = dbcon.getConnect();
             stmt = con.createStatement();
             
             String sql1 = "select p.pid,pl.name,p.unit_price,p.quantity,p.remaining "
                     + "from  purchase_details p left join plant pl on p.pid = pl.id"
                     + "where p.id = "+id+" ";
             ResultSet rst = stmt.executeQuery(sql1);
             
             while(rst.next())
             {
                 ArrayList row = new ArrayList();
                 
                 row.add(rst.getString(1)+":"+rst.getString(2));
                 row.add(rst.getString(3));
                 row.add(rst.getString(4));
                 row.add(rst.getString(5));
                 data.add(row);
             }
         }
         catch (Exception e) {
             System.out.println(e);
         }
         finally {
             if (con != null) {
                 dbcon.closeConnection(con);
             }
         }
         
         
         return data;
     }
     
     public ArrayList<ArrayList> getSupplyDetails(int id)
     {
         ArrayList<ArrayList> data = new ArrayList<>();
         
         Connection con = null;
         Statement stmt;
         
         try {
             
             con = dbcon.getConnect();
             stmt = con.createStatement();
             
             String type = "";
             
             String sql = "SELECT type FROM supply WHERE id = "+id+" ";
             String sql1="";
             ResultSet rst1 = stmt.executeQuery(sql);
             
             if(rst1.next())
             {
                 type = rst1.getString(1);
             }
             
             if(type.equals("crop"))
             {
                 sql1 = "SELECT cd.pid,p.name,cd.type,cd.quantity,cd.remaining "
                         + "FROM crop_details cd LEFT JOIN plant p ON cd.pid=p.id "
                         + "WHERE cd.id = " + id + " ";
                 ResultSet rst = stmt.executeQuery(sql1);

                 while (rst.next()) {
                     ArrayList row = new ArrayList();

                     row.add(rst.getString(1) + ":" + rst.getString(2));
                     row.add(rst.getString(3));
                     row.add(rst.getString(4));
                     row.add(rst.getString(5));
                     data.add(row);
                 }
             }
             else if(type.equals("purchase"))
             {
                 sql1 = "select p.pid,pl.name,p.length,p.type,p.quantity,p.remaining "
                     + "from  purchase_details p left join plant pl on p.pid = pl.id "
                     + "where p.id = "+id+" ";
                 ResultSet rst = stmt.executeQuery(sql1);

                 while (rst.next()) {
                     ArrayList row = new ArrayList();
                     
                     if(!rst.getString(3).equalsIgnoreCase(""))
                         row.add(rst.getString(1) + ":" + rst.getString(2) + "(" + rst.getString(3) + ")");
                     else
                         row.add(rst.getString(1) + ":" + rst.getString(2));

                     row.add(rst.getString(4));
                     row.add(rst.getString(5));
                     row.add(rst.getString(6));
                     data.add(row);
                 }
             }
             
             
             
         }
         catch (Exception e) {
             System.out.println(e);
         }
         finally {
             if (con != null) {
                 dbcon.closeConnection(con);
             }
         }
         
         
         return data;
     }
}

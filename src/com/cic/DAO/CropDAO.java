/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.DAO;

import com.cic.data.DBConnManager;
import com.cic.entity.Crop;
import com.cic.entity.CropDetail;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author NIroshan
 */
public class CropDAO {
    
     DBConnManager dbcon;

    public CropDAO() {
        dbcon = new DBConnManager();
    }

     
    public int getID()
    {
        int id = 0;
        
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql1 = "SELECT MAX(id) FROM supply";
            
            ResultSet rst = stmt.executeQuery(sql1);
            
            if(rst.next())
                id = rst.getInt(1);
            id++;
            
            
            
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return id;
    }

    public boolean save(Crop crop) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql2;
            int estate = crop.getEstate().getId();
            
            String sql= "INSERT INTO supply(date,type) "
                    + "VALUES()";
            
            
            String sql1 = "INSERT INTO crop(eid) VALUES("+estate+")";
            stmt.addBatch(sql);
            
            ArrayList<CropDetail> cropDetails = crop.getCropDetails();
            
            for(int i=0;i<cropDetails.size();i++)
            {
                String pid = cropDetails.get(i).getVariety().getID();
                int quantity = cropDetails.get(i).getVariety().getQuantity();
                
                sql2 = "INSERT INTO crop_details(id,pid,quantity) VALUES(LAST_INSERT_ID(),'"+pid+"',"+quantity+")";
                stmt.addBatch(sql2);
            }
           

            
              stmt.executeBatch();

                saved = true;

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return saved;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.DAO;

import com.cic.data.DBConnManager;
import com.cic.data.Messages;
import com.cic.entity.Client;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author NIroshan
 */
public class ClientDAO {

    DBConnManager dbcon;

    public ClientDAO() {
        dbcon = new DBConnManager();
    }

    public boolean save(Client client) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();

            String fname = client.getFname();
            String lname = client.getLname();
            String tel = client.getTel();
            String country = client.getCountry();

            String sql = "INSERT INTO client(fname,lname,tel,country)"
                    + "VALUES('" + fname + "','" + lname + "','" + tel + "','" + country + "')";

            int i = stmt.executeUpdate(sql);

            if (i > 0) {
                saved = true;
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return saved;
    }

    public boolean delete(Client client) {
        boolean delete = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            int id = client.getId();
            String sql = "DELETE FROM client WHERE id="+id+" ";
            stmt.executeUpdate(sql);
            delete = true;

        }
        catch(MySQLIntegrityConstraintViolationException ex)
        {
            Messages.showError("Detetion error", "Cannot delete selected record.This record refere to another tables.\nDeletion of this record may lead to data loss");
        }
        catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return delete;
    }

    public boolean update(Client client) {
        boolean update = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            int id = client.getId();
            String fname = client.getFname();
            String lname = client.getLname();
            String tel = client.getTel();
            String country = client.getCountry();
            
            
            String sql = "UPDATE client SET fname='"+fname+"',lname = '"+lname+"',tel='"+tel+"',country='"+country+"'"
                    + "WHERE id="+id+"   ";
            stmt.executeUpdate(sql);
            update = true;

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return update;
    }

    public boolean existing(Client client) {
        boolean existing = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String fname = client.getFname();
            String lname = client.getLname();
            
            String sql = "SELECT * FROM client WHERE fname ='"+fname+"' AND lname= '"+lname+"'    ";
            ResultSet rst =  stmt.executeQuery(sql);
            
            if(rst.next())
            {
                existing = true;
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return existing;
    }

    public Vector getClients() {
        Vector clients = new Vector();

        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT fname,lname FROM client";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                clients.add(rst.getString(1)+" "+rst.getString(2) );
               
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return clients;
    }

    public int getClientID(String client) {
        
        
        int id =0;
        Connection con = null;
        Statement stmt;

        String[] name = client.split(" ");
        
        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id FROM client WHERE fname ='"+name[0]+"'  AND lname = '"+name[1]+"'    ";

            ResultSet rst = stmt.executeQuery(sql);
            if(rst.next())
            {
                id = rst.getInt(1);
            }
            
             
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return id;
    }
    
    public Vector getAllClients() {
        Vector clients = new Vector();

        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id,fname,lname,tel,country FROM client";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                Vector v = new Vector();
                v.add(rst.getString(1) );
                v.add(rst.getString(2) );
                v.add(rst.getString(3) );
                v.add(rst.getString(4) );
                v.add(rst.getString(5) );
                
                clients.add(v);
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return clients;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cic.DAO;

import com.cic.data.DBConnManager;
import com.cic.data.Messages;
import com.cic.entity.Supplier;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author NIroshan
 */
public class SupplierDAO {
    
    DBConnManager dbcon;

    public SupplierDAO() {
        dbcon = new DBConnManager();
    }
    
    
    public boolean save(Supplier supplier) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            String fname = supplier.getFname();
            String lname = supplier.getLname();
            String tel = supplier.getTel();
            String email = supplier.getEmail();
            
            String sql = "INSERT INTO supplier(fname,lname,tel,email)"
                    + "VALUES('"+fname+"','"+lname+"','"+tel+"','"+email+"')";
            
            int i = stmt.executeUpdate(sql);
            
            if(i>0)
                saved=true;
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return saved;
    }
    
    public boolean delete(Supplier supplier) {
        boolean delete = false;
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            int supplierID = supplier.getId();
            
            String sql = "DELETE FROM supplier WHERE id = "+supplierID+" ";
            stmt.executeUpdate(sql);
            delete = true;
            
        }
        catch(MySQLIntegrityConstraintViolationException ex)
        {
            Messages.showError("Detetion error", "Cannot delete selected record.This record refere to another tables.\nDeletion of this record may lead to data loss");
        }
        catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return delete;
    }
    
    public boolean update(Supplier sup) {
        boolean update = false;
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            int id = sup.getId();
            String fname = sup.getFname();
            String lname = sup.getLname();
            String tel = sup.getTel();
            String email = sup.getEmail();
            
            String sql = "UPDATE  supplier "
                    + "SET fname='"+fname+"',lname='"+lname+"',tel='"+tel+"',email='"+email+"' "
                    + "WHERE id ="+id+"  ";
            stmt.executeUpdate(sql);
            update = true;
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return update;
    }
    
    public boolean existing(Supplier sup) {
        boolean existing = false;
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            String fname = sup.getFname();
            String lname = sup.getLname();
            
            
            String sql = "SELECT * FROM supplier WHERE fname='"+fname+"' AND lname='"+lname+"'   ";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            if(rst.next())
            {
                existing = true;
            }
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return existing;
    }
    
    public Vector getSupplierNames() {
        Vector suppliers = new Vector();

        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id,fname,lname FROM supplier";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                suppliers.add(rst.getString(1)+":"+rst.getString(2)+" "+rst.getString(3) );
               
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return suppliers;
    }
    
    public int getSupplierID(String supplier) {
        int id = 0;
        Connection con = null;
        Statement stmt;

        String[] name = supplier.split(" ");

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id FROM supplier WHERE fname ='" + name[0] + "'  AND lname = '" + name[1] + "'    ";

            ResultSet rst = stmt.executeQuery(sql);
            if (rst.next()) {
                id = rst.getInt(1);
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return id;
    }
    
     public Vector getAllSupplier() {
        Vector suppliers = new Vector();

        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id,fname,lname,tel,email FROM supplier";

            ResultSet rst = stmt.executeQuery(sql);

            while (rst.next()) {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));
                v.add(rst.getString(4));
                v.add(rst.getString(5));
                
                suppliers.add(v);
               
            }
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return suppliers;
    }

}

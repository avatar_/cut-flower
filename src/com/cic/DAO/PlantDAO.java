/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.DAO;

import com.cic.data.DBConnManager;
import com.cic.data.Messages;
import com.cic.entity.Plant;
import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;

/**
 *
 * @author NIroshan
 */
public class PlantDAO {

    DBConnManager dbcon;

    public PlantDAO() {
        dbcon = new DBConnManager();
    }

    public boolean save(Plant plant) {
        boolean saved = false;
        Connection con = null;
        Statement stmt;

        try {

            String id = plant.getID();
            String name = plant.getName();

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "INSERT INTO plant(id,name) VALUES('" + id + "','" + name + "')";
            
            int i = stmt.executeUpdate(sql);
            
            if(i > 0 )
                saved = true;
        }
        catch(MySQLIntegrityConstraintViolationException ex)
        {
            Messages.showError("Existing ID", "Existing plant in the database.Please select a different plant ID.");
        }
        catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return saved;
    }

    public boolean delete(Plant plant) {
        boolean delete = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String id = plant.getID();
            
            String sql = "DELETE FROM plant WHERE id = '"+id+"' ";
            stmt.executeUpdate(sql);
            delete = true;

        }
         catch(MySQLIntegrityConstraintViolationException ex)
        {
            Messages.showError("Detetion error", "Cannot delete selected record.This record refere to another tables.\nDeletion of this record may lead to data loss");
        }
        catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return delete;
    }

    public boolean update(Plant plant) {
        boolean update = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String id = plant.getID();
            String name = plant.getName();
            int branch = plant.getEstate().getId();
                    
                    
            String sql = "UPDATE plant SET name = '"+name+"'  WHERE id='"+id+"' ";
            stmt.executeUpdate(sql);
            update = true;

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return update;
    }

    public boolean existing(Plant plant) {
        boolean existing = false;
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String name = plant.getName();
            String sql = "SELECT * FROM plant WHERE name = '"+name+"' ";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            if(rst.next())
            {
                existing = true;
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return existing;
    }

    public String getName(String id) {
        String name = "";
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT name FROM plant WHERE id='" + id + "' ";
            ResultSet rst = stmt.executeQuery(sql);
            if (rst.next()) {
                name = rst.getString(1).toString();
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return name;
    }
    public Vector findPlant(String name) {
     
     Vector data = new Vector();
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id,name FROM plant WHERE name LIKE '" + name + "%' ";
            ResultSet rst = stmt.executeQuery(sql);
            while(rst.next())
            {
                data.add(rst.getString(1)+":"+rst.getString(2));
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return data;
    }
    public Vector getAllPlants() {
     
     Vector data = new Vector();
        Connection con = null;
        Statement stmt;

        try {

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT id,name FROM plant";
            ResultSet rst = stmt.executeQuery(sql);
            while(rst.next())
            {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                data.add(v);
            }

        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return data;
    }
    
    public boolean assignPlantToBranch(Plant plant)
    {
         boolean saved = false;
        Connection con = null;
        Statement stmt;

        try {

            String id = plant.getID();
            int branch = plant.getEstate().getId();
            int quantity = plant.getQuantity();

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "INSERT INTO estate_plant(pid,estate,quantity) VALUES('"+id+"','" +branch+ "',"+quantity+")";
            
            int i = stmt.executeUpdate(sql);
            saved = true;
        }
        catch(MySQLIntegrityConstraintViolationException ex)
        {
            Messages.showError("Existing ID", "Existing plant in the database.Please select a different plant ID.");
        }
        catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return saved;
    }
    
     public Vector getAllPlantBranches()
    {
        Vector data = new Vector();
        Connection con = null;
        Statement stmt;

        try {

            

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT pb.pid,p.name,b.name,pb.quantity "
                    + "FROM plant_branch pb LEFT JOIN plant p ON p.id = pb.pid LEFT JOIN branch b ON b.id = pb.branch";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            while(rst.next())
            {
                Vector v = new Vector();
                v.add(rst.getString(1));
                v.add(rst.getString(2));
                v.add(rst.getString(3));
                v.add(rst.getString(4));
                
                data.add(v);
            }
        }
        catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }

        return data;
    }
    
    public Vector getAssignedPlants()
    {
        Vector data = new Vector();
        
        Connection con = null;
        Statement stmt;

        try {

            

            con = dbcon.getConnect();
            stmt = con.createStatement();
            String sql = "SELECT ep.estate,e.name,ep.pid, p.name,ep.quantity "
                    + "FROM estate_plant ep "
                    + "LEFT JOIN estate e ON ep.estate = e.id "
                    + "LEFT JOIN plant p ON ep.pid = p.id";
            
            ResultSet rst = stmt.executeQuery(sql);
            
            while(rst.next())
            {
                Vector v = new Vector();
                v.add(rst.getString(1)+":"+rst.getString(2));
                v.add(rst.getString(3)+":"+rst.getString(4));
                v.add(rst.getString(5));
                
                data.add(v);
            }
            
            
        }
        catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        return data;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.DAO;

import com.cic.data.DBConnManager;
import com.cic.entity.PackingMaterialPurchase;
import com.cic.entity.PackingMaterialPurchaseDetail;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author NIroshan
 */
public class PackingMaterialPurchaseDAO {
    
    DBConnManager dbcon;

    public PackingMaterialPurchaseDAO() {
        dbcon = new DBConnManager();
    }
    
    public int getPurchaseID()
    {
        int id=0;
        
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql1 = "SELECT MAX(id) FROM pm_purchase";
            
            ResultSet rst = stmt.executeQuery(sql1);
            
            if(rst.next())
                id = rst.getInt(1);
            
            id++;
            
        }
        catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        return id;
    }
    
    
    public boolean save(PackingMaterialPurchase packingMatPur)
    {
        boolean saved = false;
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql1,sql2;
            
            String date = packingMatPur.getDate();
            int sid = packingMatPur.getSupplier().getId();
            double grandTotal = packingMatPur.getTotal();
                    
            sql1="INSERT INTO pm_purchase(date,s_id,total) VALUES('"+date+"',"+sid+","+grandTotal+")";
            System.out.println(sql1);
            stmt.addBatch(sql1);
            
            for(PackingMaterialPurchaseDetail detail : packingMatPur.getPackingMaterialPurchaseDetail())
            {
                 
                String pid = detail.getPackingMaterial().getID();
                int quantity = detail.getQuantity();
                double unitPrice = detail.getUnitPrice();
                double total = detail.getTotal();
                
                sql2 = "INSERT INTO pm_purchase_details(id,pm_id,quantity,unit_price,total) "
                        + "VALUES(LAST_INSERT_ID(),'"+pid+"',"+quantity+","+unitPrice+","+total+" )";
                
                stmt.addBatch(sql2);
            }

            stmt.executeBatch();
            saved = true;
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return saved;
    }
    
   public ArrayList<ArrayList> getPurchases(String from, String to)
    {
        ArrayList<ArrayList> data = new ArrayList();
        
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql1 = "SELECT p.id,s.fname,s.lname,p.date,p.total FROM pm_purchase p , supplier s WHERE p.s_id = s.id";
            
            ResultSet rst = stmt.executeQuery(sql1);
            
            while(rst.next())
            {
                ArrayList arr = new ArrayList();
                arr.add(rst.getString(1));
                arr.add(rst.getString(2)+" "+rst.getString(3));
                arr.add(rst.getString(4));
                arr.add(rst.getString(5));
                
                data.add(arr);
            }
            
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return data;
    }
}

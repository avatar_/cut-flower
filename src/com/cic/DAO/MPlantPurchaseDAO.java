/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.DAO;

import com.cic.data.DBConnManager;
import com.cic.entity.MPlantPurchase;
import com.cic.entity.MPlantPurchaseDetail;
import java.sql.Connection;
import java.sql.Statement;

/**
 *
 * @author NIroshan
 */
public class MPlantPurchaseDAO {

    private DBConnManager dbcon;
    
    public MPlantPurchaseDAO() {
        dbcon = new DBConnManager();
    }
    
    public boolean save(MPlantPurchase mPlantPurchase)
    {
        boolean saved = false;
        
        Connection con = null;
        Statement stmt;
        
        try {
            
            con = dbcon.getConnect();
            stmt = con.createStatement();
            
            String sql1,sql2;
            
            int estate = mPlantPurchase.getEstate().getId();
            double grandTotal = mPlantPurchase.getTotal();
            int isd = mPlantPurchase.getSupplier().getId();
            String date = mPlantPurchase.getDate();
            
            sql1 = "INSERT INTO plant_grow(estate,date,total,sid) VALUES("+estate+",'"+date+"',"+grandTotal+","+isd+")";
            stmt.addBatch(sql1);
            
            for(MPlantPurchaseDetail detail : mPlantPurchase.getPlantPurchaseDetails())
            {
                String pid = detail.getPlant().getID();
                int quantity = detail.getQuantity();
                double unitPrice = detail.getUnitPrice();
                double total = detail.getTotal();
                
                sql2 = "INSERT INTO plant_grow_details(id,pid,quantity,unitPrice,total) "
                        + "VALUES(LAST_INSERT_ID(),'"+pid+"',"+quantity+","+unitPrice+","+total+")";
                stmt.addBatch(sql2);
            }
            
            stmt.executeBatch();
            saved = true;
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if (con != null) {
                dbcon.closeConnection(con);
            }
        }
        return saved;
    }
    
}

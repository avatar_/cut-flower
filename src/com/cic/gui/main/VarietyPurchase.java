/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.gui.main;

import com.cic.DAO.CropDAO;
import com.cic.DAO.PlantDAO;
import com.cic.DAO.SupplierDAO;
import com.cic.DAO.SupplyDAO;
import com.cic.data.Messages;
import com.cic.entity.PurchaseDetail;
import com.cic.entity.Supply;
import com.cic.entity.Variety;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author NIroshan
 */
public class VarietyPurchase extends javax.swing.JFrame {

    
    JTextField txtPlantID;
    public VarietyPurchase() {
        initComponents();
        autoFillPID();
        setSuppliers();
        loadConsole();
        setDate();
        setID();
         this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        cmbSupplier = new javax.swing.JComboBox();
        dt = new com.toedter.calendar.JDateChooser();
        jButton4 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblPurchase = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cmbPID = new javax.swing.JComboBox();
        txtLength = new javax.swing.JTextField();
        txtUnitPrice = new javax.swing.JTextField();
        txtQuantity = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        cmbType = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cic/icons/save-icon.png"))); // NOI18N
        jButton1.setText("Save");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cic/icons/add icon.png"))); // NOI18N
        jButton2.setText("Add");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel5.setText("Date");

        jLabel6.setText("Supplier");

        dt.setDateFormatString("MMM-dd-yyyy");

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cic/icons/remove icon.png"))); // NOI18N
        jButton4.setText("Delete");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addGap(18, 18, 18)
                .addComponent(jButton4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(38, 38, 38))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(23, 23, 23)))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(cmbSupplier, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(dt, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jButton2)
                        .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(dt, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbSupplier, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tblPurchase.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Variety", "Type", "Length", "Quantity", "Unit Price", "Total"
            }
        ));
        jScrollPane1.setViewportView(tblPurchase);
        tblPurchase.getColumnModel().getColumn(1).setMaxWidth(150);
        tblPurchase.getColumnModel().getColumn(2).setMaxWidth(100);
        tblPurchase.getColumnModel().getColumn(3).setMaxWidth(100);
        tblPurchase.getColumnModel().getColumn(4).setMaxWidth(100);
        tblPurchase.getColumnModel().getColumn(5).setMaxWidth(100);

        jPanel2.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        jLabel1.setText("Variety");

        cmbPID.setEditable(true);

        jLabel2.setText("Length");

        jLabel3.setText("Unit Price");

        jLabel4.setText("Quantity");

        jLabel7.setText("ID");

        jLabel8.setText("Type");

        cmbType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Foliage", "Flower", "Plant" }));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3)
                            .addComponent(jLabel4))
                        .addGap(19, 19, 19)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtLength)
                            .addComponent(txtUnitPrice)
                            .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel7)
                            .addComponent(jLabel8))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbPID, 0, 340, Short.MAX_VALUE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(cmbType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cmbPID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(cmbType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtLength, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUnitPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 378, Short.MAX_VALUE))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 297, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        if(tblPurchase.getRowCount() > 0)
        {
            if (dt.getDate() == null) {
                Messages.showError("Invalid Date", "Invalid Date.Please enter the date in correct format");
            }
            if (cmbSupplier.getSelectedIndex() == 0) {
                Messages.showError("Invalid Supplier", "Please select the supplier");
            }
            else {
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                String date = df.format(dt.getDate());

                String supplier = cmbSupplier.getSelectedItem().toString();
                String[] supplierDet = supplier.split(":");
                int sid = Integer.parseInt(supplierDet[0]);

                double grandTotal = 0;

                DefaultTableModel dftm = (DefaultTableModel) tblPurchase.getModel();

                int rowCount = dftm.getRowCount();
                ArrayList<PurchaseDetail> purchaseDetails = new ArrayList<>();

                for (int i = 0; i < rowCount; i++) {
                    String plant = dftm.getValueAt(i, 0).toString();
                    String type = dftm.getValueAt(i, 1).toString();
                    String length =  dftm.getValueAt(i, 2).toString();
                    int quantity = Integer.parseInt(dftm.getValueAt(i, 3).toString());
                    double unitPrice = Double.parseDouble(dftm.getValueAt(i, 4).toString());
                    double total = Double.parseDouble(dftm.getValueAt(i, 5).toString());
                    String[] plantDet = plant.split(":");


                    PurchaseDetail detail = new PurchaseDetail();
                    
                    Variety variety = new Variety();
                    variety.setID(plantDet[0]);
                    variety.setType(type);
                    variety.setLength(length);
                    variety.setQuantity(quantity);
                    variety.setUnitPrice(unitPrice);
                    
                    detail.setVariety(variety);
                    detail.setTotal(total);

//                    detail.setLength(length);
//                    detail.setQuantity(quantity);
//                    detail.setUnitPrice(unitPrice);

                    purchaseDetails.add(detail);

                    grandTotal += total;

                }

                com.cic.entity.Purchase purchase = new com.cic.entity.Purchase();

                purchase.setTotal(grandTotal);
                purchase.setSid(sid);
                purchase.setPurchaseDetail(purchaseDetails);

                Supply supply = new Supply();
                supply.setDate(date);
                supply.setType("purchase");
                supply.setPurchase(purchase);

                boolean saved = new SupplyDAO().save(supply);
                if (saved) {
                    dftm.setRowCount(0);
                    setID();
                }
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed

        int plantIndex = cmbPID.getSelectedIndex();
        
   
        if (plantIndex == -1) {
            Messages.showError("Invalid variety ID", "Invalid variety ID.\nPlease select a valid variety ID.");
        }
        else if (!valideText(txtUnitPrice)) {
            Messages.showError("Empty unit price field", "Unit price field can not be empty.\nPlease enter the unit price");
        }
        else if (!valideText(txtQuantity)) {
            Messages.showError("Empty quantity field", "Quantity field can not be empty.\nPlease enter the quantity");
        }
        else
        {
            String pid = cmbPID.getSelectedItem().toString();
            String length = txtLength.getText();
            String type = cmbType.getSelectedItem().toString();
            int quantity = Integer.parseInt(txtQuantity.getText());
            double unitPrice = Double.parseDouble(txtUnitPrice.getText());
            double total = quantity * unitPrice;

            if(!existingVariety(pid,type,length))
            {
            
                DefaultTableModel dftm = (DefaultTableModel) tblPurchase.getModel();

                Object[] row = {pid,type, length, quantity, unitPrice, total};
                dftm.addRow(row);
            }
            else {
                Messages.showError("Existing variety", "Existing variety in the table.\nPlease enter a different variety");

            }
            clearTexts();

        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:

        DefaultTableModel dftm = (DefaultTableModel) tblPurchase.getModel();
        int row = tblPurchase.getSelectedRow();
        if (row > -1) {
            dftm.removeRow(row);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    public final void autoFillPID()// auto fill packing material item ID
    {
        txtPlantID = (JTextField) cmbPID.getEditor().getEditorComponent();
        txtPlantID.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                int ij = e.getKeyCode();

                if ((ij < 37 || ij > 40) && ij != 10) {
                    String id = txtPlantID.getText();
                    Vector data = new Vector();
                    data = new PlantDAO().findPlant(id);
                    cmbPID.setModel(new DefaultComboBoxModel(data));
                    cmbPID.setSelectedIndex(-1);
                    ((JTextField) cmbPID.getEditor().getEditorComponent()).setText(id);
                    cmbPID.showPopup();
                }
            }
        });
    }
    
    public void setSuppliers() {
        Vector suppliers = new SupplierDAO().getSupplierNames();
        suppliers.add(0, "Select Supplier");
        cmbSupplier.setModel(new DefaultComboBoxModel(suppliers));
    }
    
    
    
    private void loadConsole() {
        this.addWindowListener(new java.awt.event.WindowAdapter() {

            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {

                setVisible(false);
                new Console().setVisible(true);
            }
        });
    }
    
    private void clearTexts()
    {
        cmbPID.removeAllItems();
        txtLength.setText("");
        txtQuantity.setText("");
        txtUnitPrice.setText("");
    }
    
     private void setID()
    {
        int id = new CropDAO().getID();
        txtID.setText(String.format("%04d", id));
    }
    private boolean valideText(JTextField txt) {
        boolean valid = false;

        if (!txt.getText().isEmpty()) {
            valid = true;
        }

        return valid;
    }
    
    private boolean existingVariety(String vid,String ptype,String plength)
    {
        boolean existing = false;
        int rowCount = tblPurchase.getRowCount();

        for(int i =0;i<rowCount;i++)
        {
            String pid = tblPurchase.getValueAt(i, 0).toString();
            String type = tblPurchase.getValueAt(i, 1).toString();
            String length = tblPurchase.getValueAt(i, 2).toString();
            if(pid.equalsIgnoreCase(vid) && type.equalsIgnoreCase(ptype) && length.equalsIgnoreCase(plength))
            {
                existing = true;
            }
        }
        return existing;
    }
    
    private void setDate() {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cl = Calendar.getInstance();
            String d = df.format(cl.getTime());
            dt.setDate(df.parse(d));
//            dtVPExportDT.setDate(df.parse(dt));
        }
        catch (ParseException ex) {
            System.out.println(ex);
        }
    }
    
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VarietyPurchase.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VarietyPurchase.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VarietyPurchase.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VarietyPurchase.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new VarietyPurchase().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmbPID;
    private javax.swing.JComboBox cmbSupplier;
    private javax.swing.JComboBox cmbType;
    private com.toedter.calendar.JDateChooser dt;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tblPurchase;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtLength;
    private javax.swing.JTextField txtQuantity;
    private javax.swing.JTextField txtUnitPrice;
    // End of variables declaration//GEN-END:variables
}

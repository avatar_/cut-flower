/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.gui.update;

import com.cic.DAO.*;
import com.cic.data.Messages;
import com.cic.entity.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author NIroshan
 */
public class UpdateExport extends javax.swing.JFrame {

    public UpdateExport() {
        initComponents();
        setExports();
        autoFillPID();
        autoFillPID2();
        autoFillPackingMID();
        setDate();
         this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tblExports = new javax.swing.JTable();
        jPanel12 = new javax.swing.JPanel();
        jLabel15 = new javax.swing.JLabel();
        cmbVariety = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        txtLength = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txtUnitPrice = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtQuantity = new javax.swing.JTextField();
        cmbCorp = new javax.swing.JComboBox();
        dtHarvest = new com.toedter.calendar.JDateChooser();
        jPanel5 = new javax.swing.JPanel();
        dtExport = new com.toedter.calendar.JDateChooser();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblExportSumm = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblExportSumm1 = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        dtExport1 = new com.toedter.calendar.JDateChooser();
        jScrollPane4 = new javax.swing.JScrollPane();
        tblPackMaterial = new javax.swing.JTable();
        jPanel11 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        cmbPacMatID3 = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        txtPackQuantity3 = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tblExports1 = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        dtExport2 = new com.toedter.calendar.JDateChooser();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblExportSumm2 = new javax.swing.JTable();
        jPanel13 = new javax.swing.JPanel();
        jLabel17 = new javax.swing.JLabel();
        cmbVariety1 = new javax.swing.JComboBox();
        jLabel20 = new javax.swing.JLabel();
        txtLength1 = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txtUnitPrice1 = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txtQuantity1 = new javax.swing.JTextField();
        cmbPurchase = new javax.swing.JComboBox();
        dtHarvest1 = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 1, true));

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cic/icons/save-icon.png"))); // NOI18N
        jButton1.setText("Save");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cic/icons/table-remove-icon 48.png"))); // NOI18N
        jButton2.setText("Delete");

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/cic/icons/table-add-icon.png"))); // NOI18N
        jButton3.setText("Add");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1)
                .addGap(18, 18, 18)
                .addComponent(jButton2)
                .addGap(18, 18, 18)
                .addComponent(jButton3)
                .addContainerGap(371, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2)
                    .addComponent(jButton3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tblExports.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Harvest", "Variety", "Length", "Quantity", "Unit Price", "Total"
            }
        ));
        jScrollPane6.setViewportView(tblExports);

        jPanel12.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel12.setPreferredSize(new java.awt.Dimension(565, 685));

        jLabel15.setText("Variety");

        cmbVariety.setEditable(true);

        jLabel16.setText("Length");

        jLabel18.setText("Unit Price");

        jLabel19.setText("Quantity");

        dtHarvest.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dtHarvestPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel15)
                            .addComponent(jLabel16)
                            .addComponent(jLabel19)
                            .addComponent(jLabel18))
                        .addGap(20, 20, 20)
                        .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUnitPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLength, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbVariety, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel12Layout.createSequentialGroup()
                        .addComponent(cmbCorp, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(dtHarvest, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cmbCorp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dtHarvest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbVariety, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txtLength, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txtQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUnitPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18))
                .addGap(23, 23, 23))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        dtExport.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dtExportPropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(202, Short.MAX_VALUE)
                .addComponent(dtExport, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dtExport, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tblExportSumm.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Date", "Total", "Country"
            }
        ));
        tblExportSumm.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblExportSummMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tblExportSumm);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 1027, Short.MAX_VALUE)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 377, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Export", jPanel2);

        tblExportSumm1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Date", "Total", "Country"
            }
        ));
        tblExportSumm1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblExportSumm1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblExportSumm1);

        jPanel6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        dtExport1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dtExport1PropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(202, Short.MAX_VALUE)
                .addComponent(dtExport1, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dtExport1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tblPackMaterial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Packing Material", "Quantity"
            }
        ));
        jScrollPane4.setViewportView(tblPackMaterial);

        jPanel11.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel12.setText("Packing Material ID");

        cmbPacMatID3.setEditable(true);

        jLabel13.setText("Quantity");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13))
                .addGap(41, 41, 41)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbPacMatID3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addComponent(txtPackQuantity3, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(cmbPacMatID3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txtPackQuantity3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 623, Short.MAX_VALUE))
                    .addComponent(jScrollPane4))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 364, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Packing Material", jPanel3);

        tblExports1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Purchase", "Variety", "Length", "Quantity", "Unit Price", "Total"
            }
        ));
        jScrollPane7.setViewportView(tblExports1);

        jPanel7.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        dtExport2.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dtExport2PropertyChange(evt);
            }
        });

        tblExportSumm2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Date", "Total", "Country"
            }
        ));
        tblExportSumm2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblExportSumm2MouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tblExportSumm2);
        tblExportSumm2.getColumnModel().getColumn(0).setMaxWidth(100);

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(dtExport2, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dtExport2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel13.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jPanel13.setPreferredSize(new java.awt.Dimension(565, 685));

        jLabel17.setText("Variety");

        cmbVariety1.setEditable(true);

        jLabel20.setText("Length");

        jLabel21.setText("Unit Price");

        jLabel22.setText("Quantity");

        dtHarvest1.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                dtHarvest1PropertyChange(evt);
            }
        });

        jLabel1.setText("Purchase");

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel17)
                    .addComponent(jLabel20)
                    .addComponent(jLabel22)
                    .addComponent(jLabel21)
                    .addComponent(jLabel1))
                .addGap(18, 18, 18)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtUnitPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtLength1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQuantity1, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbVariety1, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(152, Short.MAX_VALUE))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addComponent(cmbPurchase, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(dtHarvest1, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38))))
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(dtHarvest1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbVariety1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel17))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtLength1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtQuantity1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel22))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtUnitPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21)))
                    .addGroup(jPanel13Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel1)))
                .addGap(23, 23, 23))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jPanel13, javax.swing.GroupLayout.PREFERRED_SIZE, 517, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 1012, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel13, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 349, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("purchased Variety", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 575, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void dtHarvestPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dtHarvestPropertyChange
        if (dtHarvest.getDate() != null) {
            System.out.println(dtHarvest.getDate());
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            String date = df.format(dtHarvest.getDate());



            Vector data = new SupplyDAO().getCrops(date);
            cmbCorp.setModel(new DefaultComboBoxModel(data));
        }
    }//GEN-LAST:event_dtHarvestPropertyChange

    private void dtExportPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dtExportPropertyChange

        if (dtExport.getDate() != null) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String date = df.format(dtExport.getDate());

            findExports(date, tblExportSumm);
        }
    }//GEN-LAST:event_dtExportPropertyChange

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        
        
         int tab = jTabbedPane1.getSelectedIndex();

        if (tab == 0) {

            int selectedIndex = cmbVariety.getSelectedIndex();
            if(cmbCorp.getSelectedIndex() == -1)
            {
                Messages.showError("Invalid Harvest", "Invalid Harvest ID.\nPlease select a valid harvest.");
            }
            else if (selectedIndex == -1) {
                Messages.showError("Invalid Product", "Invalid Product ID.\nPlease select a valid product ID.");
            }
            else if (!valideText(txtQuantity)) {
                Messages.showError("Empty quantity field", "Quantity field can not be empty.\nPlease enter the quantity");
            }
            else if (!valideText(txtUnitPrice)) {
                Messages.showError("Empty unit price field", "Unit price field can not be empty.\nPlease enter the unit price");
            }
            else if (!valideText(txtLength)) {
                Messages.showError("Empty length field", "Length field can not be empty.\nPlease enter the length");
            }
            else {
                String crop = cmbCorp.getSelectedItem().toString();
                String plant = cmbVariety.getSelectedItem().toString();
                int quantity = Integer.parseInt(txtQuantity.getText());
                double unitPrice = Double.parseDouble(txtUnitPrice.getText());
                double total = unitPrice * quantity;
                double length = Double.parseDouble(txtLength.getText());

                String[] cropArr = crop.split(":");
                int HID = Integer.parseInt(cropArr[0]);

                String[] plantArr = plant.split(":");
                String PID = plantArr[0];

                if (!existingPlant(PID, length, HID)) {

                    Object[] row = {crop, plant, length, quantity, unitPrice, total};
                    DefaultTableModel dftm = (DefaultTableModel) tblExports.getModel();
                    dftm.addRow(row);
                }

            }

        }
        
        else if(tab == 1)
        {
            int i = cmbPacMatID3.getSelectedIndex();

            if (i == -1) {
                Messages.showError("Invalid packing material ID", "Invalid packing material ID.\nPlease select a valid packing material");
            }
            else if (!valideText(txtPackQuantity3)) {
                Messages.showError("Empty weight field", "Empty weight field.\nWeight field can not be empty.Please enter the weight.");
            }
            else {
                String packMat = cmbPacMatID3.getSelectedItem().toString();
                String quantity = txtPackQuantity3.getText();
                Object[] row = {packMat, quantity};
                DefaultTableModel dftm = (DefaultTableModel) tblPackMaterial.getModel();
                dftm.addRow(row);
            }
        }
        else if(tab == 2)
        {
            if(cmbPurchase.getSelectedIndex() == -1)
            {
                
            }
            else if (cmbVariety1.getSelectedIndex() == -1) {
                Messages.showError("Invalid Product", "Invalid Product ID.\nPlease select a valid product ID.");
            }
            else if (!valideText(txtQuantity1)) {
                Messages.showError("Empty quantity field", "Quantity field can not be empty.\nPlease enter the quantity");
            }
            else if (!valideText(txtUnitPrice1)) {
                Messages.showError("Empty unit price field", "Unit price field can not be empty.\nPlease enter the unit price");
            }
            else if (!valideText(txtLength1)) {
                Messages.showError("Empty length field", "Length field can not be empty.\nPlease enter the length");
            }
            else {
                String purchase = cmbPurchase.getSelectedItem().toString();
                String plant = cmbVariety1.getSelectedItem().toString();
                int quantity = Integer.parseInt(txtQuantity1.getText());
                double unitPrice = Double.parseDouble(txtUnitPrice1.getText());
                double total = quantity * unitPrice;
                double length = Double.parseDouble(txtLength1.getText());

                String[] purchArr = purchase.split(":");
                String[] plantArr = plant.split(":");

                int PurID = Integer.parseInt(purchArr[0]);
                String plantID = plantArr[0];


                if (!existingPlant(plantID, length, PurID)) {
                    Object[] row = {purchase, plant, length, quantity, unitPrice, total};
                    DefaultTableModel dftm = (DefaultTableModel) tblExports1.getModel();
                    dftm.addRow(row);
                }
            }
        }
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
    }//GEN-LAST:event_jButton3ActionPerformed

    private void tblExportSummMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblExportSummMouseClicked
        
        int row = tblExportSumm.getSelectedRow();
        if(row > -1)
        {
            int id = Integer.parseInt(tblExportSumm.getValueAt(row, 0).toString());

            ArrayList<ArrayList> data = new SalesDAO().getSalesDetails(id, "crop");
            DefaultTableModel dftm = (DefaultTableModel) tblExports.getModel();
            dftm.setRowCount(0);

            for (int i = 0; i < data.size(); i++) {
                Object[] ob = (Object[]) data.get(i).toArray();
                dftm.addRow(ob);
            }

        }
    }//GEN-LAST:event_tblExportSummMouseClicked

    private void tblExportSumm1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblExportSumm1MouseClicked

        int row = tblExportSumm1.getSelectedRow();
        if(row > -1)
        {
            int eid = Integer.parseInt(tblExportSumm1.getValueAt(row, 0).toString());

            DefaultTableModel dftm = (DefaultTableModel) tblPackMaterial.getModel();
            dftm.setRowCount(0);
            ArrayList<ArrayList> data = new SalesDAO().getPackingMaterials(eid);

            for (int i = 0; i < data.size(); i++) {
                Object[] ob = data.get(i).toArray();
                dftm.addRow(ob);
            }
        }
    }//GEN-LAST:event_tblExportSumm1MouseClicked

    private void tblExportSumm2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblExportSumm2MouseClicked
        
        int row = tblExportSumm2.getSelectedRow();
        if (row > -1) {
            int id = Integer.parseInt(tblExportSumm2.getValueAt(row, 0).toString());

            ArrayList<ArrayList> data = new SalesDAO().getSalesDetails(id, "purchase");
            DefaultTableModel dftm = (DefaultTableModel) tblExports1.getModel();
            dftm.setRowCount(0);

            for (int i = 0; i < data.size(); i++) {
                Object[] ob = (Object[]) data.get(i).toArray();
                dftm.addRow(ob);
            }
        }
    }//GEN-LAST:event_tblExportSumm2MouseClicked

    private void dtExport1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dtExport1PropertyChange

        
        if (dtExport1.getDate() != null) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String date = df.format(dtExport1.getDate());

            findExports(date, tblExportSumm1);
        }
        
        // TODO add your handling code here:
    }//GEN-LAST:event_dtExport1PropertyChange

    private void dtExport2PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dtExport2PropertyChange
        // TODO add your handling code here:
        if (dtExport2.getDate() != null) {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            String date = df.format(dtExport2.getDate());

            findExports(date, tblExportSumm2);
        }
    }//GEN-LAST:event_dtExport2PropertyChange

    private void dtHarvest1PropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_dtHarvest1PropertyChange

         if (dtHarvest1.getDate() != null) {
            System.out.println(dtHarvest1.getDate());
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

            String date = df.format(dtHarvest1.getDate());



            ArrayList data = new SupplyDAO().getPurchases(date);
             System.out.println(data.size());
            cmbPurchase.setModel(new DefaultComboBoxModel(data.toArray()));
        }
        
        
        
        
    }//GEN-LAST:event_dtHarvest1PropertyChange

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        int tab = jTabbedPane1.getSelectedIndex();
        
        if(tab == 0 )
        {
            updateExport();
        }
        else if (tab == 1)
        {
            updateIncludedPackingMaterial();
        }
        else if(tab ==2)
        {
            includePurchasedVariety();
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }
        catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UpdateExport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UpdateExport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UpdateExport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UpdateExport.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new UpdateExport().setVisible(true);
            }
        });
    }
    
    
    private void setDate() {
        try {
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Calendar cl = Calendar.getInstance();
            String dt = df.format(cl.getTime());
            System.out.println(dt);
            dtExport.setDate(df.parse(dt));
            dtExport1.setDate(df.parse(dt));
            dtExport2.setDate(df.parse(dt));
            dtHarvest.setDate(df.parse(dt));
            dtHarvest1.setDate(df.parse(dt));
        }
        catch (ParseException ex) {
            System.out.println(ex);
        }
    }
    private void setExports() {

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");

        Calendar cal = Calendar.getInstance();
        String date = df.format(cal.getTime());

        System.out.println(date);
        ArrayList<ArrayList> exports = new SalesDAO().getSales(date,"export");




        DefaultTableModel dftm = (DefaultTableModel) tblExportSumm.getModel();
        DefaultTableModel dftm2 = (DefaultTableModel) tblExportSumm1.getModel();
        DefaultTableModel dftm3 = (DefaultTableModel) tblExportSumm2.getModel();
        dftm.setRowCount(0);
        dftm2.setRowCount(0);
        dftm3.setRowCount(0);
//        dftm2.setRowCount(0);

        for (int i = 0; i < exports.size(); i++) {
            Object[] row = exports.get(i).toArray();
            dftm.addRow(row);
            dftm2.addRow(row);
            dftm3.addRow(row);
//            dftm2.addRow(row);

        }
    }
    private boolean existingPlant(String pid, double plength, int HID) {
        boolean existing = false;

        JTable tbl = new JTable();

        int tab = jTabbedPane1.getSelectedIndex();

        if (tab == 0) {
            tbl = tblExports;
        }
        else if (tab == 2) {
            tbl = tblExports1;
        }


        int rows = tbl.getRowCount();

        for (int i = 0; i < rows; i++) {
            String[] productID = tbl.getValueAt(i, 1).toString().split(":");
            String[] harvest = tbl.getValueAt(i, 0).toString().split(":");
            double length = Double.parseDouble(tbl.getValueAt(i, 2).toString());
            int harvestID = Integer.parseInt(harvest[0]);

            if (productID[0].equals(pid) && length == plength && harvestID == HID) {
                existing = true;
            }
        }
        return existing;
    }
    private void findExports(String date, JTable tbl) {

        System.out.println(date);
        ArrayList<ArrayList> exports = new SalesDAO().getSales(date,"export");

        DefaultTableModel dftm = (DefaultTableModel) tbl.getModel();
        dftm.setRowCount(0);

        for (int i = 0; i < exports.size(); i++) {
            Object[] row = exports.get(i).toArray();
            dftm.addRow(row);

        }
    }
    private boolean valideText(JTextField txt) {
        boolean valid = false;

        if (!txt.getText().isEmpty()) {
            valid = true;
        }

        return valid;
    }
    
    private void updateIncludedPackingMaterial()
    {
        int exportRow = tblExportSumm1.getSelectedRow();
        int exportID = Integer.parseInt(tblExportSumm1.getValueAt(exportRow, 0).toString());


        int pacMatRowCount = tblPackMaterial.getRowCount();
        DefaultTableModel dftm = (DefaultTableModel) tblPackMaterial.getModel();
        ArrayList<PackingMaterial> packingMatArr = new ArrayList<>();


        for (int i = 0; i < pacMatRowCount; i++) {
            String pacMat = tblPackMaterial.getValueAt(i, 0).toString();
            String[] pacMatArr = pacMat.split(",");

            String pacMatID = pacMatArr[0];

            int quantity = Integer.parseInt(tblPackMaterial.getValueAt(i, 1).toString());

            PackingMaterial packingMaterial = new PackingMaterial();

            packingMaterial.setID(pacMatID);
            packingMaterial.setQuantity(quantity);

            packingMatArr.add(packingMaterial);
        }

        com.cic.entity.Sales export = new com.cic.entity.Sales();

        export.setId(exportID);
        export.setPackingMaterial(packingMatArr);

        boolean saved = new SalesDAO().includePackingMaterial(export);
        if (saved) {
            dftm.setRowCount(0);
        }
    }
    
    private void includePurchasedVariety()
    {
        int exportRow = tblExportSumm2.getSelectedRow();
        int exportID = Integer.parseInt(tblExportSumm2.getValueAt(exportRow, 0).toString());

        DefaultTableModel dftm = (DefaultTableModel) tblExports1.getModel();



        int rowCount = dftm.getRowCount();
        double grandTotal = 0;
        ArrayList<SalesDetail> exportDetailArray = new ArrayList<>();

        for (int i = 0; i < rowCount; i++) {
            String purchase = dftm.getValueAt(i, 0).toString();
            String[] cropArr = purchase.split(":");

            String plant = dftm.getValueAt(i, 1).toString();
            String[] plantArr = plant.split(":");

            String length = dftm.getValueAt(i, 2).toString();
            int quantity = Integer.parseInt(dftm.getValueAt(i, 3).toString());
            double unitPrice = Double.parseDouble(dftm.getValueAt(i, 4).toString());
            double total = unitPrice * quantity;

            grandTotal += total;

            Plant plantOB = new Plant();
            plantOB.setID(plantArr[0]);

            Supply supply = new Supply();
            supply.setId(Integer.parseInt(cropArr[0]));

            SalesDetail exportDetail = new SalesDetail();

//            exportDetail.setLength(length);
//            exportDetail.setPlant(plantOB);
//            exportDetail.setQuantity(quantity);
//            exportDetail.setSupply(supply);
//            exportDetail.setTotal(total);
//            exportDetail.setUnitPrice(unitPrice);

            exportDetailArray.add(exportDetail);
        }

        com.cic.entity.Sales export = new com.cic.entity.Sales();
        export.setId(exportID);
        export.setSalesDetails(exportDetailArray);
        export.setTotal(grandTotal);

        boolean saved = new SalesDAO().includePlantPurchases(export);
        if (saved) {
            dftm.setRowCount(0);
        }
    }
    private void updateExport()
    {
        int row = tblExportSumm.getSelectedRow();
//       int client = new ClientDAO().getClientID(cmbClient.getSelectedItem().toString());
        int eid = Integer.parseInt(tblExportSumm.getValueAt(row, 0).toString());
        double grandTotal = 0;

        //export details
        DefaultTableModel dftm = (DefaultTableModel) tblExports.getModel();
        int rowCount = dftm.getRowCount();

        ArrayList<SalesDetail> exportDetailArray = new ArrayList<>();

        for (int i = 0; i < rowCount; i++) {
            String crop = dftm.getValueAt(i, 0).toString();
            String[] cropArr = crop.split(":");

            String plant = dftm.getValueAt(i, 1).toString();
            String[] plantArr = plant.split(":");

            String length = dftm.getValueAt(i, 2).toString();
            int quantity = Integer.parseInt(dftm.getValueAt(i, 3).toString());
            double unitPrice = Double.parseDouble(dftm.getValueAt(i, 4).toString());
            double total = unitPrice * quantity;

            grandTotal += total;

            Plant plantOB = new Plant();
            plantOB.setID(plantArr[0]);

            Supply supply = new Supply();
            supply.setId(Integer.parseInt(cropArr[0]));

            SalesDetail exportDetail = new SalesDetail();
//
//            exportDetail.setLength(length);
//            exportDetail.setPlant(plantOB);
//            exportDetail.setQuantity(quantity);
//            exportDetail.setSupply(supply);
//            exportDetail.setTotal(total);
//            exportDetail.setUnitPrice(unitPrice);

            exportDetailArray.add(exportDetail);
        }

//        Client clientOB = new Client();
//        clientOB.setId(client);

        com.cic.entity.Sales export = new com.cic.entity.Sales();
        export.setId(eid);
//        export.setClient(clientOB);
//        export.setDate(date);
        export.setSalesDetails(exportDetailArray);
        export.setTotal(grandTotal);

        boolean saved = new SalesDAO().update(export);
        if (saved) {
            dftm.setRowCount(0);
        }
        setExports();
    }
    
    
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox cmbCorp;
    private javax.swing.JComboBox cmbPacMatID3;
    private javax.swing.JComboBox cmbPurchase;
    private javax.swing.JComboBox cmbVariety;
    private javax.swing.JComboBox cmbVariety1;
    private com.toedter.calendar.JDateChooser dtExport;
    private com.toedter.calendar.JDateChooser dtExport1;
    private com.toedter.calendar.JDateChooser dtExport2;
    private com.toedter.calendar.JDateChooser dtHarvest;
    private com.toedter.calendar.JDateChooser dtHarvest1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable tblExportSumm;
    private javax.swing.JTable tblExportSumm1;
    private javax.swing.JTable tblExportSumm2;
    private javax.swing.JTable tblExports;
    private javax.swing.JTable tblExports1;
    private javax.swing.JTable tblPackMaterial;
    private javax.swing.JTextField txtLength;
    private javax.swing.JTextField txtLength1;
    private javax.swing.JTextField txtPackQuantity3;
    private javax.swing.JTextField txtQuantity;
    private javax.swing.JTextField txtQuantity1;
    private javax.swing.JTextField txtUnitPrice;
    private javax.swing.JTextField txtUnitPrice1;
    // End of variables declaration//GEN-END:variables

    JTextField txtPlantID,txtPackingMID,txtPlantID2;
    
    
    
    
    public final void autoFillPID2()// auto fill packing material item ID
    {
        txtPlantID2 = (JTextField) cmbVariety1.getEditor().getEditorComponent();
        txtPlantID2.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                int ij = e.getKeyCode();

                if ((ij < 37 || ij > 40) && ij != 10) {
                    String id = txtPlantID2.getText();
                    Vector data = new Vector();
                    data = new PlantDAO().findPlant(id);
                    cmbVariety1.setModel(new DefaultComboBoxModel(data));
                    cmbVariety1.setSelectedIndex(-1);
                    ((JTextField) cmbVariety1.getEditor().getEditorComponent()).setText(id);
                    cmbVariety1.showPopup();
                }
            }
        });
    }
    
     public final void autoFillPID()// auto fill packing material item ID
    {
        txtPlantID = (JTextField) cmbVariety.getEditor().getEditorComponent();
        txtPlantID.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                int ij = e.getKeyCode();

                if ((ij < 37 || ij > 40) && ij != 10) {
                    String id = txtPlantID.getText();
                    Vector data = new Vector();
                    data = new PlantDAO().findPlant(id);
                    cmbVariety.setModel(new DefaultComboBoxModel(data));
                    cmbVariety.setSelectedIndex(-1);
                    ((JTextField) cmbVariety.getEditor().getEditorComponent()).setText(id);
                    cmbVariety.showPopup();
                }
            }
        });
    }
    public final void autoFillPackingMID()// auto fill packing material item ID
    {
        txtPackingMID = (JTextField) cmbPacMatID3.getEditor().getEditorComponent();
        txtPackingMID.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                int ij = e.getKeyCode();

                if ((ij < 37 || ij > 40) && ij != 10) {
                    String id = txtPackingMID.getText();
                    Vector data = new Vector();
                    data = new PackingMaterialDAO().findPMID(id);
                    cmbPacMatID3.setModel(new DefaultComboBoxModel(data));
                    cmbPacMatID3.setSelectedIndex(-1);
                    ((JTextField) cmbPacMatID3.getEditor().getEditorComponent()).setText(id);
                    cmbPacMatID3.showPopup();
                }
            }
        });

    }
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
}

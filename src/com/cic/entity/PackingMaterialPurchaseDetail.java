/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

/**
 *
 * @author NIroshan
 */
public class PackingMaterialPurchaseDetail {
    
    private int ID;
    private PackingMaterial packingMaterial;
    private int quantity;
    private double unitPrice;
    private double total;

    public PackingMaterialPurchaseDetail() {
    }

    public PackingMaterialPurchaseDetail(int ID, PackingMaterial packingMaterial, int quantity, double unitPrice, double total) {
        this.ID = ID;
        this.packingMaterial = packingMaterial;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.total = total;
    }

    public PackingMaterial getPackingMaterial() {
        return packingMaterial;
    }

    public void setPackingMaterial(PackingMaterial packingMaterial) {
        this.packingMaterial = packingMaterial;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
    
    
    
}

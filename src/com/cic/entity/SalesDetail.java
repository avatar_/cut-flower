/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

/**
 *
 * @author NIroshan
 */
public class SalesDetail {
 
    private Variety variety;
    private double total;
    private Supply supply;

    public SalesDetail() {
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    public Supply getSupply() {
        return supply;
    }

    public void setSupply(Supply supplyID) {
        this.supply = supplyID;
    }

    public Variety getVariety() {
        return variety;
    }

    public void setVariety(Variety variety) {
        this.variety = variety;
    }
    
}

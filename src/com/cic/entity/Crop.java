/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

import java.util.ArrayList;

/**
 *
 * @author NIroshan
 */
public class Crop {
    private int id;
    private Estate estate;
    
    private ArrayList<CropDetail> cropDetails;
    
    public Crop() {
    }

    
    public Crop(int id, Estate eid) {
        this.id = id;
        this.estate = eid;
    }

    public Crop(int id, Estate eid, ArrayList<CropDetail> cropDetails) {
        this.id = id;
        this.estate = eid;
        this.cropDetails = cropDetails;
    }
    

    public Estate getEstate() {
        return estate;
    }

    public void setEstate(Estate eid) {
        this.estate = eid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<CropDetail> getCropDetails() {
        return cropDetails;
    }

    public void setCropDetails(ArrayList<CropDetail> cropDetails) {
        this.cropDetails = cropDetails;
    }
    
    
}

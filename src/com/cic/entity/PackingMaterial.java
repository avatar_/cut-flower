/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

/**
 *
 * @author NIroshan
 */
public class PackingMaterial {
    
    private String ID;
    private String name;
    private int quantity;
    

    public PackingMaterial() {
    }

    public PackingMaterial(String ID, String name, int quantity) {
        this.ID = ID;
        this.name = name;
        this.quantity = quantity;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

}

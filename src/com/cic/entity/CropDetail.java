/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

import java.util.ArrayList;

/**
 *
 * @author NIroshan
 */
public class CropDetail {
    private int id;
    private Variety variety;

    public CropDetail() {
    }

    public CropDetail(int id, Variety plant) {
        this.id = id;
        this.variety = plant;
    }

    public CropDetail(Variety plant) {
        this.variety = plant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Variety getVariety() {
        return variety;
    }

    public void setVariety(Variety variety) {
        this.variety = variety;
    }
}

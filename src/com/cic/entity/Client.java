/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

/**
 *
 * @author NIroshan
 */
public class Client {
    
    private int id;
    private String fname;
    private String lname;
    private String tel;
    private String country;

    public Client() {
    }

    public Client(int id, String fname, String lname, String tel, String country) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.tel = tel;
        this.country = country;
    }

    public Client(String fname, String lname, String tel, String country) {
        this.fname = fname;
        this.lname = lname;
        this.tel = tel;
        this.country = country;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

/**
 *
 * @author NIroshan
 */
public class PurchaseDetail {
    private int id;
    
    private Variety variety;
    
//    private double length;
//    private int quantity;
//    private double unitPrice;
    private double total;

    public PurchaseDetail() {
    }

    public PurchaseDetail(double length, int quantity, double unitPrice, double total) {
//        this.length = length;
//        this.quantity = quantity;
//        this.unitPrice = unitPrice;
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

     
    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public Variety getVariety() {
        return variety;
    }

    public void setVariety(Variety variety) {
        this.variety = variety;
    }

}

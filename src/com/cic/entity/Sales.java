/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

import java.util.ArrayList;

/**
 *
 * @author NIroshan
 */
public class Sales {
    private int id;
    private String date;
    private double total;
    private Client client;
    private ArrayList<SalesDetail> sales;
    private ArrayList<PackingMaterial> packingMaterial;
    private String type;

    public Sales() {
    }
    public Sales(String date, double total, Client client) {
        this.date = date;
        this.total = total;
        this.client = client;
    }

    public Sales(String date, double total, Client client, ArrayList<SalesDetail> salesDetails) {
        this.date = date;
        this.total = total;
        this.client = client;
        this.sales = salesDetails;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ArrayList<SalesDetail> getSalesDetails() {
        return sales;
    }

    public void setSalesDetails(ArrayList<SalesDetail> salesDetails) {
        this.sales = salesDetails;
    }

    public ArrayList<PackingMaterial> getPackingMaterial() {
        return packingMaterial;
    }

    public void setPackingMaterial(ArrayList<PackingMaterial> packingMaterial) {
        this.packingMaterial = packingMaterial;
    }

    public ArrayList<SalesDetail> getSales() {
        return sales;
    }

    public void setSales(ArrayList<SalesDetail> sales) {
        this.sales = sales;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    
}

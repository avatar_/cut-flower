/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

import java.util.ArrayList;

/**
 *
 * @author NIroshan
 */
public class Purchase {
    
    private int id;
    private double total;
    private int sid;
    
    private ArrayList<PurchaseDetail> purchaseDetail;

    public Purchase() {
    }

    public Purchase(double total, int sid, ArrayList<PurchaseDetail> purchaseDetail) {
        this.total = total;
        this.sid = sid;
        this.purchaseDetail = purchaseDetail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<PurchaseDetail> getPurchaseDetail() {
        return purchaseDetail;
    }

    public void setPurchaseDetail(ArrayList<PurchaseDetail> purchaseDetail) {
        this.purchaseDetail = purchaseDetail;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
}

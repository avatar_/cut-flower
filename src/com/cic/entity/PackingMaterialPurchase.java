/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

import java.util.ArrayList;

/**
 *
 * @author NIroshan
 */
public class PackingMaterialPurchase {

    private  int ID;
    private String date;
    private Supplier supplier;
    private double total;
    private ArrayList<PackingMaterialPurchaseDetail> packingMaterialPurchaseDetail;

    public PackingMaterialPurchase() {
    }

    
    
    public PackingMaterialPurchase(int ID, String date, Supplier supplier, double total, ArrayList<PackingMaterialPurchaseDetail> packingMaterialPurchaseDetail) {
        this.ID = ID;
        this.date = date;
        this.supplier = supplier;
        this.total = total;
        this.packingMaterialPurchaseDetail = packingMaterialPurchaseDetail;
    }

    public PackingMaterialPurchase(String date, Supplier supplier, double total, ArrayList<PackingMaterialPurchaseDetail> packingMaterialPurchaseDetail) {
        this.date = date;
        this.supplier = supplier;
        this.total = total;
        this.packingMaterialPurchaseDetail = packingMaterialPurchaseDetail;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<PackingMaterialPurchaseDetail> getPackingMaterialPurchaseDetail() {
        return packingMaterialPurchaseDetail;
    }

    public void setPackingMaterialPurchaseDetail(ArrayList<PackingMaterialPurchaseDetail> packingMaterialPurchaseDetail) {
        this.packingMaterialPurchaseDetail = packingMaterialPurchaseDetail;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }
    
}

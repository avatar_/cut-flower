/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

import java.util.ArrayList;

/**
 *
 * @author NIroshan
 */
public class MPlantPurchase {
    private int id;
    private Estate estate;
    private String date;
    private double total;
    private Supplier supplier;
    private ArrayList<MPlantPurchaseDetail> plantPurchaseDetails;

    public MPlantPurchase() {
    }

    public MPlantPurchase(int id, Estate estate, double total, Supplier supplier, ArrayList<MPlantPurchaseDetail> plantPurchaseDetails) {
        this.id = id;
        this.estate = estate;
        this.total = total;
        this.supplier = supplier;
        this.plantPurchaseDetails = plantPurchaseDetails;
    }
    
    public MPlantPurchase(int id, Estate estate, double total, Supplier supplier) {
        this.id = id;
        this.estate = estate;
        this.total = total;
        this.supplier = supplier;
    }

    public Estate getEstate() {
        return estate;
    }

    public void setEstate(Estate estate) {
        this.estate = estate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public ArrayList<MPlantPurchaseDetail> getPlantPurchaseDetails() {
        return plantPurchaseDetails;
    }

    public void setPlantPurchaseDetails(ArrayList<MPlantPurchaseDetail> plantPurchaseDetails) {
        this.plantPurchaseDetails = plantPurchaseDetails;
    }
    
}

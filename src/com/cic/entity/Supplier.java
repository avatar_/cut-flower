/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

/**
 *
 * @author NIroshan
 */
public class Supplier {
    private int id;
    private String fname;
    private String lname;
    private String tel;
    private String email;

    public Supplier() {
    }

    public Supplier(int id, String fname, String lname, String tel, String email) {
        this.id = id;
        this.fname = fname;
        this.lname = lname;
        this.tel = tel;
        this.email = email;
    }

    
    public Supplier(String fname, String lname, String tel, String email) {
        this.fname = fname;
        this.lname = lname;
        this.tel = tel;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

import java.util.ArrayList;

/**
 *
 * @author NIroshan
 */
public class Supply {
    private int id;
    private String date;
    private String type;
    
    
    private Purchase purchase;
    private Crop crop;

    public Supply() {
    }

    
    public Supply(String date, String type, Purchase purchase, Crop crop) {
        this.date = date;
        this.type = type;
        this.purchase = purchase;
        this.crop = crop;
    }

    public Crop getCrop() {
        return crop;
    }

    public void setCrop(Crop crop) {
        this.crop = crop;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
}

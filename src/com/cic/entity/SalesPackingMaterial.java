/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.entity;

/**
 *
 * @author NIroshan
 */
public class SalesPackingMaterial {
    
    private String pmID;
    private int eid;
    private int quantity;
    private Sales export;

    public SalesPackingMaterial() {
    }

    public SalesPackingMaterial(String pmID, int eid, int quantity) {
        this.pmID = pmID;
        this.eid = eid;
        this.quantity = quantity;
    }

    public int getEid() {
        return eid;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }

    public String getPmID() {
        return pmID;
    }

    public void setPmID(String pmID) {
        this.pmID = pmID;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Sales getExport() {
        return export;
    }

    public void setExport(Sales export) {
        this.export = export;
    }
    
}

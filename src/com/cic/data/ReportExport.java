/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cic.data;

import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

/**
 *
 * @author NIroshan
 */
public class ReportExport {

    DBConnManager dbcon;
    public ReportExport() {
        dbcon = new DBConnManager();
    }
    
    
    
    
    public void exportToExcel(String inputFile,String outputFile,HashMap parameter)
    {
        
        try {
            Connection conn = dbcon.getConnect();

            String print = JasperFillManager.fillReportToFile(inputFile, parameter, conn);
            System.out.println(print);
            JRXlsxExporter xlsExporter = new JRXlsxExporter();

            File fileP = new File(print);
            JasperPrint jasperPrint = (JasperPrint) JRLoader.loadObject(fileP);


            xlsExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
            xlsExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile + ".xlsx"));

            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            configuration.setOnePagePerSheet(true);
            configuration.setRemoveEmptySpaceBetweenColumns(true);
            configuration.setRemoveEmptySpaceBetweenRows(true);
            configuration.setDetectCellType(true);
            configuration.setWhitePageBackground(false);
            configuration.setCollapseRowSpan(true);


            xlsExporter.setConfiguration(configuration);
            System.out.println("Exporting Excel Report to ");
            xlsExporter.exportReport();
            Messages.showMessage("File was exported to location "+outputFile);
            System.out.println("Export Complete.");

        } catch (Exception e) {
            Messages.showError("Export error", "System was unable to export the file to specified location.\n"+e);
        }
        
        
        
        
        
        
        
    }
}

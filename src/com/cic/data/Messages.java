/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.cic.data;

import javax.swing.JOptionPane;

/**
 *
 * @author NIroshan
 */
public class Messages {
    
    public static void showMessage( String message)
    {
        JOptionPane.showMessageDialog(null,   message);
    }
    
    public static void showError(String title,String msg)
    {
        JOptionPane.showMessageDialog(null, msg,title,JOptionPane.ERROR_MESSAGE);
    }
    
    public static void showWarning(String title,String msg)
    {
        JOptionPane.showMessageDialog(null, msg, title,JOptionPane.WARNING_MESSAGE);
    }
    
    
}


package com.cic.data;

import java.awt.Container;
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.swing.JRViewer;


public class ReportViewer extends JFrame{
    
    public ReportViewer(String fileName, HashMap parameter) throws JRException {
        super("View Report");

        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = new DBConnManager().getConnect();

            JasperPrint print = JasperFillManager.fillReport(fileName, parameter, conn);
            JRViewer viewer = new JRViewer(print);

            Container c = getContentPane();
            c.add(viewer);
            viewer.setVisible(true);


        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | JRException ex) {
            JOptionPane.showMessageDialog(this, ex);
        }
        setBounds(10, 10, 1000, 700);
    }
}
